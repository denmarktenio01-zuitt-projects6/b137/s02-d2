package b137.tenio.s02d2;


import java.util.ArrayList;

public class ArrayLists {
    public static void main(String[] args){
        System.out.println("ArrayLists");

        //Syntax
        // ArrayLists<data_type> <identifier> = new ArrayLists<data_type>();

        ArrayList<String> nuts = new ArrayList<String>();

        nuts.add("peanut");
        System.out.println(nuts.get(0));
        nuts.add("coconut");
        System.out.println(nuts);
        nuts.remove(0);
        System.out.println(nuts);

        nuts.add("walnut");

        System.out.println(nuts);

        //Updating item(s) of an array list

        nuts.set(0,"pili-nut");
        System.out.println(nuts);

        // Removing an existing item in an array list

        nuts.remove(1);
        System.out.println(nuts);

        // Remove all items in an array list
        nuts.clear();
        System.out.println(nuts);
    }

}
